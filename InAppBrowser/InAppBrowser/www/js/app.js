var app = angular.module('starter', ['ionic']);

/* app.service('communicate',function($rootScope){
  this.communicateValue='Hello';
}); */

app.run(function($ionicPlatform, $rootScope) {
		
		if( (window.localStorage.getItem("dataRemember")) == "true" ){
			$rootScope.user = (window.localStorage.getItem("dataUser"));
			$rootScope.pass = (window.localStorage.getItem("dataUser"));
			$rootScope.isChecked = true;
		}
		
});

 
app.controller("ExampleController", function ($scope) {
	
  $scope.saveData = function(v){
	  window.localStorage.setItem("dataUser", $scope.user);
	  window.localStorage.setItem("dataPass", $scope.pass);
	  window.localStorage.setItem("dataRemember", $scope.isChecked);
  }	
	
  $scope.loadData = function(){
	$scope.user = (window.localStorage.getItem("dataUser"));
	$scope.pass = (window.localStorage.getItem("dataPass"));
	
	//change checked in checkbox
	if( (window.localStorage.getItem("dataRemember")) == "true" ){
		$scope.isChecked = true;
	}
	
	else{
		$scope.isChecked = false;
	} 
  }
			
  $scope.openInAppBrowser = function(){

	iabRef = cordova.ThemeableBrowser.open('http://google.com', '_blank', {
		statusbar: {
			color: '#ffffffff'
		},
		toolbar: {
			height: 50,
			color: '#387ef5ff'
		},
		title: {
			color: '#fff0f0ff',
			showPageTitle: true,
			staticText : 'Dimas Application'
		},
		menu: {
			//image: 'menu',
			//imagePressed: 'menu_pressed',
			wwwImage: 'img/back.png',
			wwwImagePressed: 'img/back_pressed.png',
			wwwImageDensity: 2,
			title: 'Test',
			cancel: 'Cancel',
			align: 'right',
			items: [
				{
					event: 'normalTheme',
					label: 'Normal Theme'
				},
				{
					event: 'redBcg',
					label: 'Red Background'
				},
				{
					event: 'blueBcg',
					label: 'Blue Background'
				},
				{
					event: 'yellowBcg',
					label: 'Yellow Background'
				},
				{
					event: 'bigFont',
					label: 'Big Font'
				},
				{
					event: 'smallFont',
					label: 'Small Font'
				}
			]
		},
		backButtonCanClose: true
	});
	iabRef.addEventListener('normalTheme', function(e) {
		iabRef.insertCSS({
		  code: 'body {background:#fff;color:#222;}'
		});
		iabRef.insertCSS({
		  code: '#als {font-size: small;}'
		});
	});
	iabRef.addEventListener('redBcg', function(e) {
		iabRef.insertCSS({
		  code: 'body {background-color:#ffe4e1;}'
		});
	});
	iabRef.addEventListener('blueBcg', function(e) {
		iabRef.insertCSS({
		  code: 'body {background-color:#c6e2ff;}'
		});
	});
	iabRef.addEventListener('yellowBcg', function(e) {
		iabRef.insertCSS({
		  code: 'body {background-color:#ffff66;}'
		});
	});
	iabRef.addEventListener('bigFont', function(e) {
		iabRef.insertCSS({
		  code: '#als {font-size: 27px;}'
		});
	});
	iabRef.addEventListener('smallFont', function(e) {
		iabRef.insertCSS({
		  code: '#als {font-size: 7px;}'
		});
	});
	iabRef.addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(e) {
		console.error(e.message);
	});
	iabRef.addEventListener(cordova.ThemeableBrowser.EVT_WRN, function(e) {
		console.log(e.message);
	});
  };
 
});




